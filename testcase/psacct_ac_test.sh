#!/bin/bash
###############################################################################
# @用例ID: 20231124-163610-785828127
# @用例名称: case_name_psacct
# @用例级别: 3
# @用例标签:
# @扩展属性:
# @用例类型: 功能测试
# @自动化: 1
# @超时时间: 0
# @用例描述: 测试 psacct 工具包中的 ac 功能
###############################################################################
[ -z "$TST_TS_TOPDIR" ] && {
    TST_TS_TOPDIR="$(realpath "$(dirname "$0")/..")"
    export TST_TS_TOPDIR
}
source "${TST_TS_TOPDIR}/tst_common/lib/common.sh" || exit 1
###############################################################################

g_tmpdir="$(mktemp -d)"

tc_setup() {
    msg "this is tc_setup"

    # @预置条件: 安装psacct软件包
    dnf install -y psacct
    assert_true rpm -q psacct
    return 0
}

do_test() {
    msg "this is do_test"

    echo "psacct: ac功能测试"

    # @测试步骤: 调用ac查询用户登录时间
    # @预期结果: ac打印用户时间
    ac
    assert_true [ $? -eq 0 ]
    ac -d
    assert_true [ $? -eq 0 ]
    ac -p
    assert_true [ $? -eq 0 ] 
    

    return 0
}

tc_teardown() {
    msg "this is tc_teardown"
    rm -rfv "$g_tmpdir" || return 1
    return 0
}

###############################################################################
tst_main "$@"
###############################################################################
