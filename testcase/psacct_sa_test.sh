#!/bin/bash
###############################################################################
# @用例ID: 20231124-163610-785828127
# @用例名称: case_name_psacct
# @用例级别: 3
# @用例标签:
# @扩展属性:
# @用例类型: 功能测试
# @自动化: 1
# @超时时间: 0
# @用例描述: 测试 psacct 工具包中的 lastcomm 功能
###############################################################################
[ -z "$TST_TS_TOPDIR" ] && {
    TST_TS_TOPDIR="$(realpath "$(dirname "$0")/..")"
    export TST_TS_TOPDIR
}
source "${TST_TS_TOPDIR}/tst_common/lib/common.sh" || exit 1
###############################################################################

g_tmpdir="$(mktemp -d)"

tc_setup() {
    msg "this is tc_setup"

    # @预置条件: 安装psacct软件包，并启动记录
    dnf install -y psacct
    assert_true rpm -q psacct
    accton on
    assert_true [ $? -eq 0 ]
    return 0
}

do_test() {
    msg "this is do_test"

    echo "psacct: sa功能测试"

    # @测试步骤: 调用sa查询用户命令
    # @预期结果: sa输出用户命令
    sa
    assert_true [ $? -eq 0 ]
    sa -u
    assert_true [ $? -eq 0 ]
    sa -m
    assert_true [ $? -eq 0 ]
    sa -d
    assert_true [ $? -eq 0 ]

    return 0
}

tc_teardown() {
    accton off
    assert_true [ $? -eq 0 ]
    msg "this is tc_teardown"
    rm -rfv "$g_tmpdir" || return 1
    return 0
}

###############################################################################
tst_main "$@"
###############################################################################
