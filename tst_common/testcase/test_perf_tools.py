#!/usr/bin/env python3
# coding: utf-8
import os.path
import subprocess
import sys

sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), '..'))
from lib.common import TestCase
from lib.benchmark import PerfSysBench


def prepare():
    subprocess.run(["which", "sysbench"])


def cleanup():
    pass


class PythonTestCase(TestCase):
    """
    @用例ID: 20220721-001631-159391306
    @用例名称: test_perf_tools
    @用例级别: 3
    @用例标签:
    @扩展属性:
    @用例类型: 性能测试
    @自动化: 0
    @超时时间: 0
    @用例描述: 用例测试点描述
    """

    def tc_setup(self, *args):
        # @预置条件:
        # @预置条件:
        # 断言结果是否符合预期，例如: self.skip_if_false(1 == 1)
        self.msg("this is tc_setup")

    def do_test(self, *args):
        # @测试步骤: step 1
        # @预期结果: expect of step 1
        # 断言结果是否符合预期，例如: self.assert_true(1 == 1)
        perf = PerfSysBench(name='cpu', sysbench='sysbench', testname='cpu')
        perf.prepare = prepare
        perf.cleanup = cleanup
        perf.run()
        perf.report()
        self.assert_true(len(perf.results) != 0)

        # @测试步骤: step 2

        # @测试步骤: step 3
        # @预期结果: expect of step 3

    def tc_teardown(self, *args):
        self.msg("this is tc_teardown")


if __name__ == '__main__':
    PythonTestCase().tst_main(sys.argv)
