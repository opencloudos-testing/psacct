# -*- coding: utf-8 -*-

"""
@file    : subsystem_helper.py
@time    : 2022-11-04 11:03:01
@author  : wenjiachen（陈文嘉）
@version : 1.0
@contact : wenjiachen@tencent.com
@desc    : None
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from .subsystem_cpu import SubsystemCpu
from .subsystem_cpuacct import SubsystemCpuAcct
from .subsystem_cpu_set import SubsystemCpuSet
from .subsystem_memory import SubsystemMemory


class SubsystemHelper(object):

    _SUBSYSTEMS = {
        b"cpu": SubsystemCpu,
        b"cpuacct": SubsystemCpuAcct,
        b"cpuset": SubsystemCpuSet,
        b"memory": SubsystemMemory,
    }

    @classmethod
    def get_subsystem_names(cls):
        return cls._SUBSYSTEMS.keys()

    @classmethod
    def get_subsystem_controller(cls, cgroup_node):
        subsystem_name = cgroup_node.subsystem_type
        if subsystem_name not in cls._SUBSYSTEMS.keys():
            return None
        return cls._SUBSYSTEMS[subsystem_name](cgroup_node)
