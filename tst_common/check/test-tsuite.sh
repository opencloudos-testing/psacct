#!/bin/bash
# Time: 2024-09-09 10:55:55
# Desc: 对tsuite框架进行自测
# 1、执行本脚本对tsuite框架进行自测
# 2、测试用例以test_tsuite_为前缀，用例没有参数，返回0表示成功，其他表示失败
# 3、测试脚本根据函数前缀自动发现测试用例并执行
# 4、用例执行时默认当前目录为框架顶层目录
# 5、用例执行时有顺序，按用例函数定义的先后顺序执行
# 6、测试结果汇总到check/result.log文件中

g_test_file="$(realpath "$0")"
g_test_name="$(basename "$g_test_file")"
g_top_dir="$(dirname "$(dirname "$g_test_file")")"
g_tmp_dir="$(dirname "$g_test_file")/dir.$$.tmp"
# 全局错误计数
g_error_count=0

exp_true() {
    if "$@"; then
        echo "$*"
    else
        echo "error: $* expect success but fail"
        g_error_count=$((g_error_count + 1))
    fi
}
exp_false() {
    if "$@"; then
        echo "error: $* expect fail but success"
        g_error_count=$((g_error_count + 1))
    else
        echo "$*"
    fi
}

test_tsuite_list() {
    exp_true ./tsuite list
    ./tsuite list >"$g_tmp_dir/list.log" 2>&1
    exp_true grep "test_c_testcase[[:blank:]]\+[0-9][[:blank:]]\+auto[[:blank:]./]\+testcase/test_c_testcase.c" "$g_tmp_dir/list.log"
    exp_true grep "test_perf_tools[[:blank:]]\+[0-9][[:blank:]]\+manual[[:blank:]./]\+testcase/test_perf_tools.py" "$g_tmp_dir/list.log"
    exp_true grep "test_python_testcase[[:blank:]]\+[0-9][[:blank:]]\+auto[[:blank:]./]\+testcase/test_python_testcase.py" "$g_tmp_dir/list.log"
    exp_true grep "test_shell_testcase[[:blank:]]\+[0-9][[:blank:]]\+auto[[:blank:]./]\+testcase/test_shell_testcase.sh" "$g_tmp_dir/list.log"
}

test_tsuite_compile() {
    exp_true ./tsuite compile
    exp_true test -x ./testcase/test_c_testcase.test
    exp_true test -x ./cmd/tsuite.cmd
    exp_true test -f ./kmod/kmod_common.ko
}

test_tsuite_run_one() {
    exp_true ./tsuite run testcase/test_shell_testcase.sh
    exp_true ./tsuite run testcase/test_c_testcase.c
    exp_true ./tsuite run testcase/test_perf_tools.py
    exp_true ./tsuite run testcase/test_python_testcase.py
}

test_tsuite_run_all() {
    exp_true ./tsuite run
    for i in $(seq 0 4); do
        exp_true ./tsuite run -l "$i"
    done
}

test_tsuite_clean() {
    exp_true ./tsuite clean
    exp_true test -d ./logs
    exp_false test -x ./testcase/test_c_testcase.test
    exp_false test -x ./cmd/tsuite.cmd
    exp_false test -f ./kmod/kmod_common.ko
}

test_tsuite_cleanall() {
    exp_true ./tsuite cleanall
    exp_false test -x ./testcase/test_c_testcase.test
    exp_false test -x ./cmd/tsuite.cmd
    exp_false test -f ./kmod/kmod_common.ko
    exp_false test -d ./logs
}

test_tsuite_export() {
    exp_true ./tsuite export
    exp_true test -f ./tcase.json
}

init_env() {
    cd "$g_top_dir" || return 1

    return 0
}

main() {
    local _main_ret=0

    init_env || return 1

    echo "g_top_dir=$g_top_dir"
    rm -rf "$g_tmp_dir"
    mkdir -p "$g_tmp_dir"

    # 将所有用例找出来，挨个执行
    local case_list
    case_list="$(
        grep "^[[:blank:]]*test_tsuite_.*(.*)\|^[[:blank:]]*function[[:blank:]]\+test_tsuite_.*(.*)" "$g_test_file" |
            grep -wo "test_tsuite_[_0-9a-zA-Z]\+"
    )"

    local old_error_count="$g_error_count"
    for case in $case_list; do
        cd "$g_top_dir" || _main_ret=1
        if $case; then
            if [ "$old_error_count" == "$g_error_count" ]; then
                echo "PASS     ${g_test_name}:${case}" | tee -a "$g_top_dir/check/result.log"
            else
                echo "    FAIL ${g_test_name}:${case}" | tee -a "$g_top_dir/check/result.log"
                _main_ret=1
            fi
        else
            echo "    FAIL ${g_test_name}:${case}" | tee -a "$g_top_dir/check/result.log"
            _main_ret=1
        fi
    done

    [ "$g_error_count" != "0" ] && _main_ret=1

    return $_main_ret
}

main
